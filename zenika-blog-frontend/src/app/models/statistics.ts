import {Article} from './article';
import {Input} from '@angular/core';

export class Statistics {

   listOfArticles: Article[];
   totalNumberOfTags: number;
   totalNumberOfDrafts: number;
   totalNumberOfArticles: number;
   totalNumberOfUsers: number;
   averageTagsByArticles: number;
   averageNumberOfWordsByArticles: number;


  constructor(listOfArticles: Article[], totalnumberOfTags: number, totalnumberOfDrafts: number,
              totalNumberOfArticles: number, totalNumberOfUsers: number, averageTagsByArticles: number,
              averageNumberOfWordsByArticles: number) {
    this.listOfArticles = listOfArticles;
    this.totalNumberOfTags = totalnumberOfTags;
    this.totalNumberOfDrafts = totalnumberOfDrafts;
    this.totalNumberOfArticles = totalNumberOfArticles;
    this.totalNumberOfUsers = totalNumberOfUsers;
    this.averageTagsByArticles = averageTagsByArticles;
    this.averageNumberOfWordsByArticles = averageNumberOfWordsByArticles;
  }


}
