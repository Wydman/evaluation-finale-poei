import {Tags} from './tags';

export class Article {
  id: string;
  user_id: string;
  publicCreator: string;
  state: string;
  title: string;
  content: string;
  summary: string;
  imageUrl: string;
  wordsInContent: number;
  tagsAssigned: string;

  constructor(id: string, user_id: string, publicCreator: string, state: string,
              title: string, content: string, summary: string,
              imageUrl: string, wordsInContent: number, tagsAssigned: string) {
    this.id = id;
    this.user_id = user_id;
    this.publicCreator = publicCreator;
    this.state = state;
    this.title = title;
    this.content = content;
    this.summary = summary;
    this.imageUrl = imageUrl;
    this.wordsInContent = wordsInContent;
    this.tagsAssigned = tagsAssigned;

  }
}
