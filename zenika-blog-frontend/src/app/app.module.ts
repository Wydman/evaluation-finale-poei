import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule} from '@angular/forms';
import { ArticlesListComponent } from './components/articles-list/articles-list.component';
import { ArticlesDisplayComponent } from './components/articles-display/articles-display.component';
import { HeaderComponent } from './components/header/header.component';
import { ArticleFullDisplayComponent } from './components/article-full-display/article-full-display.component';
import { CreateArticleComponent } from './components/create-article/create-article.component';
import {ArticlesService} from './services/articles.service';
import {UserService} from './services/userService';
import { SearchResultsComponent } from './components/search-results/search-results.component';
import {DraftsListComponent} from './components/drafts-list/drafts-list.component';
import {EditDraftsComponent} from './components/edit-drafts/edit-drafts.component';
import { AllStatsComponent } from './components/all-stats/all-stats.component';
import {StatisticsService} from './services/statisticsService';


const  appRoutes:Routes =[
  { path: 'articles', component:ArticlesListComponent },
  { path: 'articles/create', component:CreateArticleComponent },
  { path: 'articles/drafts', component:DraftsListComponent },
  { path: 'articles/searchresults', component:SearchResultsComponent },
  { path: 'statistics', component:AllStatsComponent }
];
@NgModule({
  declarations: [
    AppComponent,
    ArticlesListComponent,
    ArticlesDisplayComponent,
    HeaderComponent,
    ArticleFullDisplayComponent,
    CreateArticleComponent,
    EditDraftsComponent,
    DraftsListComponent,
    SearchResultsComponent,
    AllStatsComponent,
  ],
  imports: [
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes
    ),
    BrowserModule,
    FormsModule
  ],
  providers: [ArticlesService,UserService,StatisticsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
