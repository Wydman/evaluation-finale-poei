import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Article} from '../models/article';
import {Tags} from '../models/tags';

@Injectable()
export class ArticlesService {

  public titleToSearch = null;
  public articletoEdit:Article = null;
  public hasArticletoEdit:boolean = false;
  public articleJSON:JSON

  constructor(private Http: HttpClient) {
  }

  getAllArticles(): Observable<Article[]> {
    return this.Http.get<Article[]>('http://localhost:8080/articles');
  //  return this.Http.get<Article[]>('/api/articles');
  }

  getArticlesByTitle(title:string): Observable<Article[]> {
    return this.Http.post<Article[]>('http://localhost:8080/articles/search', title);
  }

  getAllTags(): Observable<Tags[]> {
    return this.Http.get<Tags[]>('http://localhost:8080/articles/tags');
  }

  createNewTag(tagName:string): Observable<Tags[]> {
    console.log(tagName);
    return this.Http.post<Tags[]>('http://localhost:8080/articles/tags', tagName);
  }

  createArticle(peonJson:JSON): Observable<Article[]> {
    console.log(peonJson);
     return this.Http.post<Article[]>('http://localhost:8080/articles/create', peonJson);
  //  return this.Http.post<Article[]>('/api/peon/generator', peonJson);
  }

  editArticle(peonJson:JSON): Observable<Article[]> {
    console.log(peonJson);
    return this.Http.post<Article[]>('http://localhost:8080/articles/edit', peonJson);
    //  return this.Http.post<Article[]>('/api/peon/generator', peonJson);
  }
}
