import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Article} from '../models/article';
import {Statistics} from '../models/statistics';
import {Tags} from '../models/tags';

@Injectable()
export class StatisticsService {

  constructor(private Http: HttpClient) {
  }

  getStatistics(): Observable<Statistics> {
    return this.Http.get<Statistics>('http://localhost:8080/statistics');
  }
}
