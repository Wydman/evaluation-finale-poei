import {EventEmitter, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {User} from '../models/user';
import {log} from 'util';

@Injectable()
export class UserService {

  currentUser: User = null;
  userConnected: boolean = false;

  constructor(private http: HttpClient) {
  }

  GetUserByName(userName: string): Observable<User> {
    return this.http.post<User>('http://localhost:8080/users/getUserByName', userName);
  }

  GetUserById(userId: string): Observable<User> {
    return this.http.post<User>('http://localhost:8080/users/id', userId);
  }

  searchUserName(userName: string): Observable<boolean> {
    return this.http.post<boolean>('http://localhost:8080/users', userName);
  }

  createNewUser(userName: string, userEmail: string, userPassword: string): Observable<User> {
    return this.http.post<User>('http://localhost:8080/users/create', new User(userName, userEmail, userPassword));
  }

  tryConnect(login:string, password: string): Observable<boolean> {
    return this.http.post<boolean>(`http://localhost:8080/users/tryConnect`, {login,password});
  }
}
