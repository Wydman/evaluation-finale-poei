import { Component, OnInit } from '@angular/core';
import {Article} from '../../models/article';
import {HttpClient} from '@angular/common/http';
import {ArticlesService} from '../../services/articles.service';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.css']
})
export class SearchResultsComponent implements OnInit {

  articles:Article[];

  constructor(private http: HttpClient,private service: ArticlesService) { }

  ngOnInit() {
    if(!this.service.titleToSearch){
      console.log(222 + this.service.titleToSearch);
      this.service.getArticlesByTitle(" ")
        .subscribe(subscribedPeonList => this.articles = subscribedPeonList)
    }
    else{
      console.log(333 + this.service.titleToSearch);
      this.service.getArticlesByTitle(this.service.titleToSearch)
        .subscribe(subscribedPeonList => this.articles = subscribedPeonList)
    }
  }

}
