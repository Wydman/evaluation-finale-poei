import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Tags} from '../../models/tags';
import {ArticlesService} from '../../services/articles.service';
import {UserService} from '../../services/userService';
import {applySourceSpanToExpressionIfNeeded} from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-create-article',
  templateUrl: './create-article.component.html',
  styleUrls: ['./create-article.component.css']
})
export class CreateArticleComponent implements OnInit {

  title: string;
  user_id: string;
  summary: string;
  imageUrl: string;
  content: string;
  state: string = 'draft';
  tagsToAssign: Tags[] = null;
  numberOfTagsToAssign: number[] = [0];

  @Input() isReadytoSubmit = false;
  @Input() submitted = false;
  @Input() tagToCreate = '';

  @Input() listOfTags: Tags[] = [];


  constructor(private articlesService: ArticlesService, private userService: UserService, private router: Router) {

  }

  ngOnInit() {
    if (this.userService.currentUser == null) {
      this.router.navigate(['articles']);
      console.log('Not logged in , you are redirected');
    } else {
      this.articlesService.getAllTags().subscribe(tags => {
        this.user_id = this.userService.currentUser.id;
        this.listOfTags = tags;
      });
    }
  }

  addTagToAssign() {
    this.numberOfTagsToAssign.push(0);
  }

  onSubmit(form: any): void {
    this.articlesService.articleJSON = form;
    console.log('you submitted value:', form);
    this.articlesService.createArticle(form).subscribe(article => {
      console.log('you received value:', article);
      this.submitted = true;
      this.router.navigate(['articles']);
    });
  }

  createNewTag(tagName: string) {
    this.articlesService.createNewTag(tagName).subscribe(tags => {
      this.articlesService.getAllTags().subscribe(tags => {
        this.listOfTags = tags;
      });
    });
  }

  updateFormStatus() {
    this.user_id = this.userService.currentUser.id;
    if (this.title != null && this.summary != null && this.content != null
      && this.title != '' && this.summary != '' && this.content != '') {
      this.isReadytoSubmit = true;
    } else {
      this.isReadytoSubmit = false;
    }
  }
}
