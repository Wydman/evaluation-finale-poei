import {Component, Input, OnInit} from '@angular/core';
import {Article} from '../../models/article';
import {Router} from '@angular/router';
import {UserService} from '../../services/userService';
import {ArticlesService} from '../../services/articles.service';

@Component({
  selector: 'app-articles-display',
  templateUrl: './articles-display.component.html',
  styleUrls: ['./articles-display.component.css']
})
export class ArticlesDisplayComponent implements OnInit {

  @Input() showFullArticle: boolean = false;
  @Input() edit: boolean = false;
  @Input() article: Article;

  constructor(private router: Router,private userService:UserService,private articlesService:ArticlesService) {

  }

  ngOnInit() {
  }

  onClick(state: boolean) {
    this.showFullArticle = state;
  }
  editDraft(){
    this.edit = true;
    this.articlesService.articletoEdit = this.article;
    this.articlesService.hasArticletoEdit = true;
  }
}
