import {Component, Input, OnInit} from '@angular/core';
import {Statistics} from '../../models/statistics';
import {StatisticsService} from '../../services/statisticsService';
import {UserService} from '../../services/userService';
import {ArticlesService} from '../../services/articles.service';

@Component({
  selector: 'app-all-stats',
  templateUrl: './all-stats.component.html',
  styleUrls: ['./all-stats.component.css']
})
export class AllStatsComponent implements OnInit {

  @Input() statistics: Statistics;
  @Input() isReadyToShowStats:boolean = false;


  constructor(private statisticsService: StatisticsService,private articlesService:ArticlesService) {
  }

  ngOnInit() {
    this.statisticsService.getStatistics()
      .subscribe(stats => {
        this.statistics = stats;
        this.isReadyToShowStats = true;
      })
  }
}
