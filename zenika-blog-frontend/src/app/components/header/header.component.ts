import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../../services/userService';
import {ArticlesService} from '../../services/articles.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Input() inputLogin: String;
  @Input() inputPassword: String;
  @Input() displayLoginMessage: boolean = false;
  @Input() userDoesntExist: boolean = null;
  @Input() createAccount: boolean = null;
  @Input() loginMessage: string = null;

  @Input() signInName: string;
  @Input() signEmail: string;
  @Input() signInPassword: string;

  @Input() searchValue: string = null;

  constructor(private userService: UserService, private router: Router, private articlesService: ArticlesService) {
  }


  tryConnect(loginOrEmail: string, password: string) {
    this.displayLoginMessage = true;

    this.userService.searchUserName(loginOrEmail).subscribe(isRegistered => {

      if (isRegistered) {
        this.userService.tryConnect(loginOrEmail, password).subscribe(passwordMatch => {
          if (passwordMatch) {
            this.loginMessage = 'vous êtes connecté';

            this.userService.GetUserByName(loginOrEmail).subscribe(user => {
              this.userService.currentUser = user;
              console.log(user);
              console.log(user.id);
              console.log(user.login);
              console.log(user.email);
              console.log(user.password);
              this.userService.userConnected = true;
            });

          } else {
            this.loginMessage = 'mauvais mot de passe  !';
          }
        });
      } else {
        this.loginMessage = 'Cet utilisateur n\'existe pas !';
        this.userDoesntExist = true;
      }
    });
  }


  createNewUser(userName: string, userEmail: string, userPassword: string) {
    this.userService.createNewUser(userName, userEmail, userPassword).subscribe(
      user => {
        console.log(user);
        if (user.id == '0') {
          this.displayLoginMessage = true;
          this.loginMessage = 'Ce pseudo est déja atribué';
        } else {
          this.createAccount = false;
          this.tryConnect(userName, userPassword);
        }
      }
    );
  }

  searchByTitle(title: string) {
    if (title === null) {
      this.articlesService.titleToSearch = '';
    } else {
      this.articlesService.titleToSearch = title;
    }
    this.router.navigate(['articles/searchresults']);
    window.location.reload();
  }

  disconnect() {
    this.userService.currentUser = null;
    this.userService.userConnected = false;
    this.router.navigate(['articles']);
  }

  ngOnInit(): void {
  }


}
