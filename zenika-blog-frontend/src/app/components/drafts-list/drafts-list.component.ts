import {Component, Input, OnInit} from '@angular/core';
import {Article} from '../../models/article';
import {HttpClient} from '@angular/common/http';
import {ArticlesService} from '../../services/articles.service';
import {UserService} from '../../services/userService';
import {Tags} from '../../models/tags';
import {Router} from '@angular/router';

@Component({
  selector: 'app-drafts-list',
  templateUrl: './drafts-list.component.html',
  styleUrls: ['./drafts-list.component.css']
})
export class DraftsListComponent implements OnInit {

  id: string;
  title: string;
  user_id: string;
  summary: string;
  imageUrl: string;
  content: string;
  tagsAssigned: string;
  state: string = 'draft';
  tagsToAssign: Tags[] = null;
  numberOfTagsToAssign: number[] = [0];

  @Input() hasArticleToEdit: boolean = false;
  @Input() isReadytoSubmit = false;
  @Input() submitted = false;
  @Input() tagToCreate = '';

  @Input() listOfTags: Tags[] = [];
  articles: Article[];

  fieldsAlreadyCompletedWithDraft: boolean = false;

  constructor(private router: Router, private service: ArticlesService, private userService: UserService) {
  }

  ngOnInit() {
    if (this.userService.currentUser == null) {
      this.router.navigate(['articles']);
      console.log('Not logged in , you are redirected');
    } else {
      this.service.getAllArticles()
        .subscribe(articles => {
          let draftList: Article[] = [];
          articles.forEach(article => {
            if (article.user_id === this.userService.currentUser.id) {
              draftList.push(article);
            }
          });
          return this.articles = draftList;
        });
    }
  }


  onSubmit(form: any): void {
    this.service.articleJSON = form;
    console.log('you submitted value:', form);
    this.service.editArticle(form).subscribe(article => {
      console.log('you received value:', article);
      this.submitted = true;
    });
    this.service.hasArticletoEdit = false;
    this.service.articletoEdit = null;
    this.router.navigate(['articles']);
  }


  setArticleInformations() {
    if (!this.fieldsAlreadyCompletedWithDraft) {
      this.user_id = this.userService.currentUser.id;
      this.title = this.service.articletoEdit.title;
      this.id = this.service.articletoEdit.id;
      this.summary = this.service.articletoEdit.summary;
      this.content = this.service.articletoEdit.content;
      this.tagsAssigned = this.service.articletoEdit.tagsAssigned;
      this.imageUrl = this.service.articletoEdit.imageUrl;
      this.fieldsAlreadyCompletedWithDraft = true;
    }
  }

  updateFormStatus() {

    if (this.title != null && this.summary != null && this.content != null
      && this.title != '' && this.summary != '' && this.content != '') {
      this.isReadytoSubmit = true;
    } else {
      this.isReadytoSubmit = false;
    }
  }

}
