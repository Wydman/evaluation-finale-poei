import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticleFullDisplayComponent } from './article-full-display.component';

describe('ArticleFullDisplayComponent', () => {
  let component: ArticleFullDisplayComponent;
  let fixture: ComponentFixture<ArticleFullDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticleFullDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleFullDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
