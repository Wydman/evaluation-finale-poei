import {Component, Input, OnInit} from '@angular/core';
import {Article} from '../../models/article';

@Component({
  selector: 'app-article-full-display',
  templateUrl: './article-full-display.component.html',
  styleUrls: ['./article-full-display.component.css']
})
export class ArticleFullDisplayComponent implements OnInit {

  @Input() article: Article;

  constructor() {
  }

  ngOnInit() {
  }

}
