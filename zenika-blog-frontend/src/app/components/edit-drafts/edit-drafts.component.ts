import {Component, OnInit} from '@angular/core';
import {Article} from '../../models/article';
import {HttpClient} from '@angular/common/http';
import {ArticlesService} from '../../services/articles.service';
import {UserService} from '../../services/userService';

@Component({
  selector: 'app-edit-drafts',
  templateUrl: './edit-drafts.component.html',
  styleUrls: ['./edit-drafts.component.css']
})
export class EditDraftsComponent implements OnInit {

  articles: Article[];

  constructor(private http: HttpClient, private service: ArticlesService, private userService: UserService) {
  }

  ngOnInit() {
    this.service.getAllArticles()
      .subscribe(articles => {
        let draftList:Article[] = [];
        articles.forEach(article =>{
          if(article.user_id === this.userService.currentUser.id){
            draftList.push(article);
          }
        });
        return this.articles = draftList;
      });
  }

}
