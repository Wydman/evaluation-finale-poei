import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDraftsComponent } from './edit-drafts.component';

describe('EditDraftsComponent', () => {
  let component: EditDraftsComponent;
  let fixture: ComponentFixture<EditDraftsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDraftsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDraftsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
