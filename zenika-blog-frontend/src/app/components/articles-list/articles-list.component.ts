import {Component, Input, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Article} from '../../models/article';
import {ArticlesService} from '../../services/articles.service';

@Component({
  selector: 'app-articles-list',
  templateUrl: './articles-list.component.html',
  styleUrls: ['./articles-list.component.css']
})
export class ArticlesListComponent implements OnInit {

  articles:Article[];

  constructor(private http: HttpClient,private service: ArticlesService) { }

  ngOnInit() {
    this.service.getAllArticles()
      .subscribe(subscribedPeonList => this.articles = subscribedPeonList)
  }
}
