#!/bin/bash
docker-compose down
cd zenika-blog-backend/
mvn package
cd ..
docker container run --name zenika-blog-postgres -p5432:5432 -e POSTGRES_PASSWORD=password -d postgres
docker build -t zenika-blog-backend zenika-blog-backend
docker build -t zenika-blog-frontend zenika-blog-frontend
docker-compose up