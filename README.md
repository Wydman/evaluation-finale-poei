# Zenika blog 

Voici les informations pour lancer le projet dans sa globalité.
Le Backend écoute sur le port **8080**. 
Le Frontend écoute sur le port **4200**. 

##Cloner le projet 

Le projet gitlab est disponible publiquement à cette adresse : `https://gitlab.com/Wydman/evaluation-finale-poei`

## Build du projet

Une fois clonné voici les choix :

- Depuis Windows : Vous pouvez double-cliquer le fichier `docker.sh` pour effectuer le lancement
de la base de données avec des données pré-enregistrées, le backend, puis le frontend.
Une fois ceci effectué, vous n'aurez plus qu'a aller sur les adresses suivantes pour
tester le backend et le frontend :
    - backend : http://localhost:8080/articles
    - frontend : http://localhost:4200/articles
    
- Depuis Linux : 
    - Rendez vous à la racine du projet depuis un invité de commande.
    - Rentrer les commandes contenues dans le docker.sh via l'invité de commande.
    - voici les commandes à éffectuer : 


```
docker-compose down
cd zenika-blog-backend/
mvn package
cd ..
docker container run --name zenika-blog-postgres -p5432:5432 -e POSTGRES_PASSWORD=password -d postgres
docker build -t zenika-blog-backend zenika-blog-backend
docker build -t zenika-blog-frontend zenika-blog-frontend
docker-compose up
``` 
Vous n'aurez plus qu'a aller sur les adresses suivantes pour
tester le backend et le frontend :
- backend : http://localhost:8080/articles
- frontend : http://localhost:4200/articles

## Test du Frontend
- Une fois sur le site, vous pourrez commencer par regarder la liste des articles existants :
pour ceci si vous n'y êtes pas déjà, cliquez sur Articles en haut à gauche du site.
seront affichés des articles résumés, pour les voirs en détails vous pouvez afficher l'article au complet en bas de chaque cartes d'articles.


- vous pourrez ensuite créer un compte pour creer un article:
pour ceci veuillez renseigner un pseudonyme aléatoire en haut à droite de l'écran, puis valider.
vous serez avertis que le compte n'existe pas et pourrez en créer un nouveau.
pour ceci, cliquez sur `Créer un compte` , rentrez votre nom, votre email et votre mot de passe et validez.

- Maintenant que vous êtes connectés, vous pouvez voir deux nouveaux bouttons sur le Header
    - `Article creation` : permet de créer de nouveaux articles
               vous pouvez ici renseigner des champs comme un titre, un créateur, un résumé, un URL d'image et le contenu.
               vous pouvez aussi choisir si votre article sera publique ou privé, avec les boutons à cocher :`draft` et `published`
               selon votre choix, votre articles une fois créé se retrouvera sur la page http://localhost:4200/articles ou sur la page http://localhost:4200/articles/drafts
                    
    - `Draft edition` : permet de modifier vos articles non publiés, puis de les publier.
                pour ceci, choisissez un de vos articles non publiés puis selectionner le boutons `Edit`
                ensuite remplissez le formulaire et appuyez sur `Generate !`

- Vous savez maintenant créer et modifier des articles, voici comment rechercher des articles :
        - en haut à droite du Header se trouve un champ de recherche, vous pouvez y rentrer un titre exact
         ou assez proche de l'un des articles puis cliquer, vous serez ensuite redirigé sur une page contenant
          tout les articles avec un titre proche.
          
- Vous pouvez aussi cliquer sur le bouton `Statistics` dans le header pour découvrir les statistiques effectives globales du projet.


## Architecture de la base de donnée :

![Alt text](Database-zenika-blog.png?raw=true "Title")