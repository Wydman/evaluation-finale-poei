DROP TABLE IF EXISTS  tags_assigned;
DROP TABLE IF EXISTS  users_views;
DROP TABLE IF EXISTS  visitors_views;
DROP TABLE IF EXISTS  tags;
DROP TABLE IF EXISTS  visitors;
DROP TABLE IF EXISTS  users;
DROP TABLE IF EXISTS  articles;


CREATE TABLE tags
(
    id              text PRIMARY KEY NOT NULL,
    name              text not null
);

CREATE TABLE users
(
        id           text PRIMARY KEY NOT NULL,
        login        text not null,
        email        text not null,
        password     text not null
);

CREATE TABLE articles
(
    id                    text PRIMARY KEY NOT NULL,
    users_id              text not null,
    public_creator        text not null,
    state                 text not null,
    title                 text not null,
    content               text not null,
    summary               text not null,
    image_url             text,
    words_in_content      int
);

CREATE TABLE visitors
(
    id             text PRIMARY KEY NOT NULL,
    ip_address     text,
    visit_duration int
);

CREATE TABLE users_views
(
    users_id             text NOT NULL REFERENCES users(id),
    articles_id          text NOT NULL REFERENCES articles(id),
    PRIMARY KEY(users_id,articles_id)
);

CREATE TABLE visitors_views
(
    visitors_id             text NOT NULL REFERENCES visitors(id),
    articles_id          text NOT NULL REFERENCES articles(id),
    PRIMARY KEY(visitors_id,articles_id)
);

CREATE TABLE tags_assigned
(
    articles_id          text NOT NULL REFERENCES articles(id),
    tags_id             text NOT NULL REFERENCES tags(id),
    PRIMARY KEY(articles_id,tags_id)
);

INSERT INTO tags VALUES('1','Technology');
INSERT INTO tags VALUES('2','Home');
INSERT INTO tags VALUES('3','Health');
INSERT INTO tags VALUES('4','Wealth');
INSERT INTO tags VALUES('5','World');
INSERT INTO tags VALUES('6','Cute');
INSERT INTO users VALUES('f0bbbcd5-f9b5-43cf-97bd-b8d1c8860be3','a','a@gmail.com','a');
INSERT INTO users VALUES('d90180fa-cce3-4e61-a880-ed4d0fd0ab53','Auteur inconnu','anonymous@anonymous','a');
INSERT INTO users VALUES('2','user3','zaegaeg@gmail.com','password');
INSERT INTO users VALUES('3','user2','zaegaeg@gmail.com','password');
INSERT INTO users VALUES('4','user1','zaegaeg@gmail.com','password');
INSERT INTO articles VALUES('1','1','Pseudo1','draft','title','content','summary','imageURL',200);
INSERT INTO articles VALUES('2','1','Pseudo2','draft','title','content','summary','imageURL',200);
INSERT INTO articles VALUES('3','3','Pseudo3','published','title','content','summary','imageURL',200);
INSERT INTO articles VALUES('4','4','Auteur inconnu','published','title','content','summary','imageURL',200)