package com.zenika.zenikablogbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZenikaBlogBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZenikaBlogBackendApplication.class, args);
	}

}
