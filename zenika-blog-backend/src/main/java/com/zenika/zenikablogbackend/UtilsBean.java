package com.zenika.zenikablogbackend;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Random;
import java.util.Scanner;

@Configuration
public class UtilsBean {

    @Bean
    public CommandLineRunner commandLineRunner(ZenikaBlogCore zenikaBlogCore) {
        return args -> {
            zenikaBlogCore.start();
        };
    }

    @Bean
    public Scanner createScanner() {
        return new Scanner(System.in);
    }

    @Bean
    public Logger logger() {
        return LoggerFactory.getLogger("QuizLogger");
    }
}
