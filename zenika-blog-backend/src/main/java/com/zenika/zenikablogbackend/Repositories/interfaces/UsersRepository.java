package com.zenika.zenikablogbackend.Repositories.interfaces;

import com.zenika.zenikablogbackend.Repositories.Users;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public interface UsersRepository extends CrudRepository<Users,String> {
    Optional<Users> findByLogin(String name);
    Optional<Users> findByEmail(String email);
}
