package com.zenika.zenikablogbackend.Repositories.interfaces;

import com.zenika.zenikablogbackend.Repositories.Tags;
import com.zenika.zenikablogbackend.Repositories.Users;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public interface TagsRepository extends CrudRepository<Tags,String> {
    Tags findByName(String name);
}
