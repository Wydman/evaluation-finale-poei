package com.zenika.zenikablogbackend.Repositories;

import javax.persistence.*;
import java.util.List;

@Entity
public class Articles {

    @Id
    private  String id;
    private  String users_id;
    private String publicCreator;
    private  String state;
    private  String title;
    private  String content;
    private  String summary;
    private  String imageUrl;
    private  int wordsInContent;

    @ManyToMany
    @JoinTable(
            name = "tags_assigned",
            joinColumns = @JoinColumn(name = "articles_id"),
            inverseJoinColumns = @JoinColumn(name = "tags_id"))
    private List<Tags> tagsAssigned;

    public Articles(String id, String users_id, String publicCreator, String state,
                    String title, String content, String summary,
                    String imageUrl, int wordsInContent,List<Tags> tagsAssigned){

        this.id = id;
        this.users_id = users_id;
        this.publicCreator = publicCreator;
        this.state = state;
        this.title = title;
        this.content = content;
        this.summary = summary;
        this.imageUrl = imageUrl;
        this.wordsInContent = wordsInContent;
        this.tagsAssigned = tagsAssigned;
    }

    Articles(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getUsers_id() {
        return users_id;
    }

    public void setUsers_id(String users_id) {
        this.users_id = users_id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getWordsInContent() {
        return wordsInContent;
    }

    public void setWordsInContent(int wordsInContent) {
        this.wordsInContent = wordsInContent;
    }

    public List<Tags> getTagsAssigned() {
        return tagsAssigned;
    }

    public void setTagsAssigned(List<Tags> tagsAssigned) {
        this.tagsAssigned = tagsAssigned;
    }

    public String getPublicCreator() {
        return publicCreator;
    }

    public void setPublicCreator(String publicCreator) {
        this.publicCreator = publicCreator;
    }
}
