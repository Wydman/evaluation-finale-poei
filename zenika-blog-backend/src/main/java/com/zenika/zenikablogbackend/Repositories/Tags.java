package com.zenika.zenikablogbackend.Repositories;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.List;

@Entity
public class Tags {
    @Id
    private  String id;
    private  String name;

    @ManyToMany(mappedBy = "tagsAssigned")
    private List<Articles> articles;

    public Tags(String id, String name) {
        this.id = id;
        this.name = name;
    }

    Tags(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Articles> getArticles() {
        return articles;
    }

    public void setArticles(List<Articles> articles) {
        this.articles = articles;
    }
}
