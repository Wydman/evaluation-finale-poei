package com.zenika.zenikablogbackend.Repositories.interfaces;

import com.zenika.zenikablogbackend.Repositories.Articles;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface ArticlesRepository extends CrudRepository<Articles, String> {
}
