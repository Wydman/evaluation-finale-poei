package com.zenika.zenikablogbackend.Representations;

public class TagsRepresentation {

    private String id;
    private String name;

    public TagsRepresentation(String id, String name) {
        this.id = id;
        this.name = name;
    }

    TagsRepresentation() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
