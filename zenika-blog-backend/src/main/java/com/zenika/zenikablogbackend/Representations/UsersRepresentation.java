package com.zenika.zenikablogbackend.Representations;

public class UsersRepresentation {
    private String id;
    private String login;
    private String email;

    public UsersRepresentation(String id, String login, String email) {
        this.id = id;
        this.login = login;
        this.email = email;
    }

    public UsersRepresentation() {

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
