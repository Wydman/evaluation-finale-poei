package com.zenika.zenikablogbackend.Representations;

import java.util.ArrayList;
import java.util.List;

public class StatisticsRepresentation {

    private Iterable<ArticlesRepresentation> listOfArticles = new ArrayList<>();
    private int totalNumberOfTags;
    private int totalNumberOfDrafts;
    private int totalNumberOfArticles;
    private int totalNumberOfUsers;
    private int averageTagsByArticles;
    private int averageNumberOfWordsByArticles;

    public StatisticsRepresentation(Iterable<ArticlesRepresentation> listOfArticles, int totalNumberOfTags,
                                    int totalNumberOfDrafts, int totalNumberOfArticles, int totalNumberOfUsers,
                                    int averageTagsByArticles, int averageNumberOfWordsByArticles) {
        this.listOfArticles = listOfArticles;
        this.totalNumberOfTags = totalNumberOfTags;
        this.totalNumberOfDrafts = totalNumberOfDrafts;
        this.totalNumberOfArticles = totalNumberOfArticles;
        this.totalNumberOfUsers = totalNumberOfUsers;
        this.averageTagsByArticles = averageTagsByArticles;
        this.averageNumberOfWordsByArticles = averageNumberOfWordsByArticles;
    }

    StatisticsRepresentation() {

    }

    public Iterable<ArticlesRepresentation> getListOfArticles() {
        return listOfArticles;
    }

    public void setListOfArticles(Iterable<ArticlesRepresentation> listOfArticles) {
        this.listOfArticles = listOfArticles;
    }

    public int getTotalNumberOfTags() {
        return totalNumberOfTags;
    }

    public void setTotalNumberOfTags(int totalNumberOfTags) {
        this.totalNumberOfTags = totalNumberOfTags;
    }

    public int getTotalNumberOfDrafts() {
        return totalNumberOfDrafts;
    }

    public void setTotalNumberOfDrafts(int totalNumberOfDrafts) {
        this.totalNumberOfDrafts = totalNumberOfDrafts;
    }

    public int getTotalNumberOfArticles() {
        return totalNumberOfArticles;
    }

    public void setTotalNumberOfArticles(int totalNumberOfArticles) {
        this.totalNumberOfArticles = totalNumberOfArticles;
    }

    public int getTotalNumberOfUsers() {
        return totalNumberOfUsers;
    }

    public void setTotalNumberOfUsers(int totalNumberOfUsers) {
        this.totalNumberOfUsers = totalNumberOfUsers;
    }

    public int getAverageTagsByArticles() {
        return averageTagsByArticles;
    }

    public void setAverageTagsByArticles(int averageTagsByArticles) {
        this.averageTagsByArticles = averageTagsByArticles;
    }

    public int getAverageNumberOfWordsByArticles() {
        return averageNumberOfWordsByArticles;
    }

    public void setAverageNumberOfWordsByArticles(int averageNumberOfWordsByArticles) {
        this.averageNumberOfWordsByArticles = averageNumberOfWordsByArticles;
    }
}
