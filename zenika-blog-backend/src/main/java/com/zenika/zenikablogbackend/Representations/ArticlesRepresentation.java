package com.zenika.zenikablogbackend.Representations;

import com.zenika.zenikablogbackend.Repositories.Tags;

import java.util.List;

public class ArticlesRepresentation {
    private String id;
    private String user_id;
    private String publicCreator;
    private String state;
    private String title;
    private String content;
    private String summary;
    private String imageUrl;
    private int wordsInContent;
    private String tagsAssigned;

    public ArticlesRepresentation(String id, String user_id, String publicCreator, String state,
                                  String title, String content, String summary,
                                  String imageUrl, int wordsInContent,String tagsAssigned) {

        this.id = id;
        this.user_id = user_id;
        this.publicCreator = publicCreator;
        this.state = state;
        this.title = title;
        this.content = content;
        this.summary = summary;
        this.imageUrl = imageUrl;
        this.wordsInContent = wordsInContent;
        this.tagsAssigned = tagsAssigned;
    }

    ArticlesRepresentation(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getWordsInContent() {
        return wordsInContent;
    }

    public void setWordsInContent(int wordsInContent) {
        this.wordsInContent = wordsInContent;
    }

    public String getTagsAssigned() {
        return tagsAssigned;
    }

    public void setTagsAssigned(String tagsAssigned) {
        this.tagsAssigned = tagsAssigned;
    }

    public String getPublicCreator() {
        return publicCreator;
    }

    public void setPublicCreator(String publicCreator) {
        this.publicCreator = publicCreator;
    }
}
