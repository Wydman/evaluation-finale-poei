package com.zenika.zenikablogbackend.Representations;

public class LoginRepresentation {
    private String login;
    private String password;

    public LoginRepresentation(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public LoginRepresentation() {

    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
