package com.zenika.zenikablogbackend.Representations;

import javax.persistence.Id;

public class UsersCreationRepresentation {

    private  String login;
    private  String email;
    private  String password;

    public UsersCreationRepresentation(String login, String email, String password) {
        this.login = login;
        this.email = email;
        this.password = password;
    }

    public UsersCreationRepresentation(){

    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
