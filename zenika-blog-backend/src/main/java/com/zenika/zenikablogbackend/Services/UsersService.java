package com.zenika.zenikablogbackend.Services;

import com.zenika.zenikablogbackend.Repositories.Users;
import com.zenika.zenikablogbackend.Repositories.interfaces.UsersRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Component
public class UsersService {

    private UsersRepository userRepository;

    public UsersService(UsersRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Users createUser(String login, String email, String password) {
        boolean alreadyRegister = false;
        for (Users user : userRepository.findAll()) {
            if (user.getLogin().equals(login)) {
                alreadyRegister = true;
                break;
            }
        }

        if (!alreadyRegister)
            return userRepository.save(new Users(UUID.randomUUID().toString(), login, email, password));
        else {
            return new Users();
        }
    }

    public Optional<Users> getUserByID(String id) {
        return userRepository.findById(id);
    }

    public Optional<Users> isUserRegistered(String name) {
        return userRepository.findByLogin(name);
    }

    public Optional<Users> getUserByName(String name) {
        return userRepository.findByLogin(name);
    }
    public Optional<Users> getUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public boolean tryConnect(String login,String password){
        boolean isConnected = false;
        for (Users user: userRepository.findAll()) {
            if(user.getLogin().equals(login) || user.getEmail().equals(login)){
                if(user.getPassword().equals(password)){
                    isConnected = true;
                }
            }
        }
        return isConnected;
    }
}