package com.zenika.zenikablogbackend.Services;

import com.zenika.zenikablogbackend.Repositories.Articles;
import com.zenika.zenikablogbackend.Repositories.Tags;
import com.zenika.zenikablogbackend.Repositories.interfaces.TagsRepository;
import com.zenika.zenikablogbackend.Representations.ArticlesRepresentation;
import com.zenika.zenikablogbackend.Domain.RepresentationsConverter;
import com.zenika.zenikablogbackend.Repositories.interfaces.ArticlesRepository;
import org.apache.commons.text.similarity.LevenshteinDistance;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@Component
public class ArticlesService {

    private ArticlesRepository articlesRepository;
    private RepresentationsConverter representationsConverter;
    private TagsRepository tagsRepository;

    ArticlesService(ArticlesRepository articlesRepository, RepresentationsConverter representationsConverter,
                    TagsRepository tagsRepository) {

        this.articlesRepository = articlesRepository;
        this.representationsConverter = representationsConverter;
        this.tagsRepository = tagsRepository;
    }

    public Iterable<ArticlesRepresentation> getAllArticlesReprensentations() {
        return representationsConverter.convertListOfArticlesToReprensentations(articlesRepository.findAll());
    }

    public Iterable<ArticlesRepresentation> searchArticlesByTitle(String title) {

        Iterable<ArticlesRepresentation> articles = representationsConverter.convertListOfArticlesToReprensentations(articlesRepository.findAll());
        List<ArticlesRepresentation> articlesAfterSearch = new ArrayList<>();

        if (!title.equals(" ")) {
            LevenshteinDistance lev = new LevenshteinDistance();
            int levDistanceCurrentlyUsed = 2;

            for (ArticlesRepresentation article : articles) {
                if (lev.apply(article.getTitle(), title) <= levDistanceCurrentlyUsed) {
                    articlesAfterSearch.add(article);
                }
            }
            return articlesAfterSearch;
        } else {
            return articles;
        }
    }


    public Iterable<Tags> getAllTags() {
        return tagsRepository.findAll();
    }

    public Iterable<Tags> saveNewTag(Tags tag) {
        tagsRepository.save(tag);
        return tagsRepository.findAll();
    }

    public ArticlesRepresentation getArticleRepresentation(String id) {
        return articlesRepository.findById(id).map(
                article -> representationsConverter.convertArticleToReprensentation(article)).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Article not found")
        );
    }

    public ArticlesRepresentation saveArticleToDatabase(ArticlesRepresentation articlesRepresentation) {
        System.out.println(articlesRepresentation.getTagsAssigned());
        articlesRepository.save(representationsConverter.convertArticleRepresentationToArticle(articlesRepresentation));
        return articlesRepresentation;
    }

    public ArticlesRepresentation deleteArticleFromDatabase(String articleId) {
        ArticlesRepresentation articlesRepresentation = articlesRepository.findById(articleId).map(
                article -> representationsConverter.convertArticleToReprensentation(article)).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Article not found")
        );
        articlesRepository.deleteById(articleId);
        return articlesRepresentation;
    }
}
