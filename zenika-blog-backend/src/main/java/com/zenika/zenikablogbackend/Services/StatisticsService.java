package com.zenika.zenikablogbackend.Services;

import com.zenika.zenikablogbackend.Domain.RepresentationsConverter;
import com.zenika.zenikablogbackend.Repositories.Articles;
import com.zenika.zenikablogbackend.Repositories.Tags;
import com.zenika.zenikablogbackend.Repositories.Users;
import com.zenika.zenikablogbackend.Repositories.interfaces.ArticlesRepository;
import com.zenika.zenikablogbackend.Repositories.interfaces.TagsRepository;
import com.zenika.zenikablogbackend.Repositories.interfaces.UsersRepository;
import com.zenika.zenikablogbackend.Representations.ArticlesRepresentation;
import com.zenika.zenikablogbackend.Representations.StatisticsRepresentation;
import org.hibernate.stat.Statistics;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class StatisticsService {

    private ArticlesRepository articlesRepository;
    private UsersRepository usersRepository;
    private TagsRepository tagsRepository;
    private RepresentationsConverter representationsConverter;

    public StatisticsService(ArticlesRepository articlesRepository,
                             UsersRepository usersRepository,
                             TagsRepository tagsRepository,
                             RepresentationsConverter representationsConverter) {

        this.articlesRepository = articlesRepository;
        this.usersRepository = usersRepository;
        this.tagsRepository = tagsRepository;
        this.representationsConverter = representationsConverter;
    }


    public StatisticsRepresentation getAllStats() {

        Iterable<ArticlesRepresentation> listOfArticles = new ArrayList<>();
        int totalNumberOfTags = 0;
        int totalNumberOfDrafts = 0;
        int totalNumberOfArticles = 0;
        int totalNumberOfUsers = 0;
        int averageTagsByArticles = 0;
        int averageNumberOfWordsByArticles = 0;


        List<Integer>listOfTagsBytArticles = new ArrayList<>();
        List<Integer>listOfNumberOfWordsByArticles = new ArrayList<>();


        for (Tags tag : tagsRepository.findAll()) {
            totalNumberOfTags++;
        }

        for (Articles article : articlesRepository.findAll()) {
            if(article.getState().equals("published")){
                totalNumberOfArticles++;
            }else{
                totalNumberOfDrafts++;
            }
            listOfTagsBytArticles.add(article.getTagsAssigned().size());
            listOfNumberOfWordsByArticles.add(article.getWordsInContent());
        }

        averageTagsByArticles = calculateAverage(listOfTagsBytArticles);
        averageNumberOfWordsByArticles = calculateAverage(listOfNumberOfWordsByArticles);

        for (Users user : usersRepository.findAll()) {
            totalNumberOfUsers++;
        }

        return new StatisticsRepresentation(representationsConverter.convertListOfArticlesToReprensentations(articlesRepository.findAll()),
                totalNumberOfTags,totalNumberOfDrafts,totalNumberOfArticles,
                totalNumberOfUsers,averageTagsByArticles,averageNumberOfWordsByArticles);
    }

    private int calculateAverage(List<Integer> ints) {
        Integer sum = 0;
        if(!ints.isEmpty()) {
            for (Integer mark : ints) {
                sum += mark;
            }
            return sum / ints.size();
        }
        return sum;
    }
}
