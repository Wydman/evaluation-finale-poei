package com.zenika.zenikablogbackend.Controllers;

import com.zenika.zenikablogbackend.Representations.ArticlesRepresentation;
import com.zenika.zenikablogbackend.Representations.StatisticsRepresentation;
import com.zenika.zenikablogbackend.Services.StatisticsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/statistics")
public class StatisticsController {

    private StatisticsService statisticsService;

    public StatisticsController(StatisticsService statisticsService){

        this.statisticsService = statisticsService;
    }

    @GetMapping("")
    public StatisticsRepresentation getAllStatistics() {
        return statisticsService.getAllStats();
    }
}
