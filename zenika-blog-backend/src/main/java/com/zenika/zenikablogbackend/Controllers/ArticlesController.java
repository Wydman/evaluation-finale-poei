package com.zenika.zenikablogbackend.Controllers;

import com.zenika.zenikablogbackend.Repositories.Tags;
import com.zenika.zenikablogbackend.Representations.ArticlesRepresentation;
import com.zenika.zenikablogbackend.Representations.TagsRepresentation;
import com.zenika.zenikablogbackend.Services.ArticlesService;
import org.hibernate.id.UUIDGenerator;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/articles")
public class ArticlesController {

    private ArticlesService articlesService;

    ArticlesController(ArticlesService articlesService) {

        this.articlesService = articlesService;
    }


    @GetMapping("")
    public Iterable<ArticlesRepresentation> getAllArticles() {
        return articlesService.getAllArticlesReprensentations();
    }

    @GetMapping("tags")
    public Iterable<TagsRepresentation> getAllTags() {
        List<TagsRepresentation> tagsRepresentations = new ArrayList<>();
        for (Tags tag : articlesService.getAllTags()) {
            tagsRepresentations.add(new TagsRepresentation(tag.getId(), tag.getName()));
        }
        return tagsRepresentations;
    }

    @PostMapping("tags")
    public Iterable<TagsRepresentation> saveNewTag(@RequestBody String tagName) {
        List<TagsRepresentation> tagsRepresentations = new ArrayList<>();
        articlesService.saveNewTag(new Tags(UUID.randomUUID().toString(), tagName));
        for (Tags tag : articlesService.getAllTags()) {
            tagsRepresentations.add(new TagsRepresentation(tag.getId(), tag.getName()));
        }
        return tagsRepresentations;
    }

    @GetMapping("/{id}")
    public ArticlesRepresentation getArticle(@PathVariable(value = "id") String id) {
        return articlesService.getArticleRepresentation(id);
    }

    @PostMapping("create")
    public ArticlesRepresentation saveArticle(@RequestBody ArticlesRepresentation articlesRepresentation) {
        System.out.println(articlesRepresentation.getUser_id());
        System.out.println(articlesRepresentation.getContent());
        System.out.println(articlesRepresentation.getTagsAssigned());
        return articlesService.saveArticleToDatabase(articlesRepresentation);
    }

    @PostMapping("search")
    public Iterable<ArticlesRepresentation> searchArticles(@RequestBody String title) {
        return articlesService.searchArticlesByTitle(title);
    }

    @PostMapping("edit")
    public ArticlesRepresentation editArticle(@RequestBody ArticlesRepresentation articlesRepresentation) {
        System.out.println(articlesRepresentation.getId());
        System.out.println(articlesRepresentation.getUser_id());
        System.out.println(articlesRepresentation.getContent());
        System.out.println(articlesRepresentation.getTagsAssigned());
        return articlesService.saveArticleToDatabase(articlesRepresentation);
    }

    @DeleteMapping("/{id}")
    public ArticlesRepresentation deleteArticle(@PathVariable(value = "id") String id) {
        return articlesService.deleteArticleFromDatabase(id);
    }
}
