package com.zenika.zenikablogbackend.Controllers;

import com.zenika.zenikablogbackend.Repositories.Users;
import com.zenika.zenikablogbackend.Repositories.interfaces.UsersRepository;
import com.zenika.zenikablogbackend.Representations.LoginRepresentation;
import com.zenika.zenikablogbackend.Representations.UsersCreationRepresentation;
import com.zenika.zenikablogbackend.Representations.UsersRepresentation;
import com.zenika.zenikablogbackend.Services.UsersService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UsersController {


    private UsersRepository userRepository;
    private UsersService userService;

    public UsersController(UsersRepository userRepository, UsersService userService) {
        this.userRepository = userRepository;
        this.userService = userService;
    }


    @GetMapping("/{id}")
    public UsersRepresentation getUser(@PathVariable(value = "id") String id) {
        Users user = userService.getUserByID(id).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found")
        );
        return new UsersRepresentation(id, user.getLogin(), user.getEmail());
    }

    @PostMapping("")
    public boolean isUserRegistered(@RequestBody String login) {
        boolean exist = false;
        for (Users user : userRepository.findAll()) {
            if (user.getLogin().equals(login) || user.getEmail().equals(login)) {
                exist = true;
            }
        }
        return exist;
    }

    @PostMapping("/getUserByName")
    public UsersRepresentation getUserByName(@RequestBody String name) {
        Users user = null;


        if (userService.getUserByEmail(name).isEmpty()) {
            user = userService.getUserByName(name).orElseThrow(
                    () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found")
            );
        }
        else{
            user = userService.getUserByEmail(name).get();
        }

        return new UsersRepresentation(user.getId(), user.getLogin(), user.getEmail());


    }

    @PostMapping("/tryConnect")
    public boolean tryConnect(@RequestBody LoginRepresentation loginRepresentation) {
        return userService.tryConnect(loginRepresentation.getLogin(), loginRepresentation.getPassword());
    }

    @GetMapping("")
    public List<UsersRepresentation> getAllUsers() {
        List<UsersRepresentation> userRepresentationList = new ArrayList<>();
        for (Users user : userRepository.findAll()) {
            userRepresentationList.add(new UsersRepresentation(user.getId(),
                    user.getLogin(),
                    user.getEmail()
            ));
        }
        return userRepresentationList;
    }


    @PostMapping("/create")
    public UsersRepresentation createUser(@RequestBody UsersCreationRepresentation usersCreationRepresentation) {
        Users user = userService.createUser(usersCreationRepresentation.getLogin(), usersCreationRepresentation.getEmail(), usersCreationRepresentation.getPassword());
        return new UsersRepresentation(user.getId(), user.getLogin(), user.getEmail());
    }

}
