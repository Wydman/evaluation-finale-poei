package com.zenika.zenikablogbackend.Domain;

import com.zenika.zenikablogbackend.Repositories.Tags;
import com.zenika.zenikablogbackend.Repositories.interfaces.TagsRepository;
import com.zenika.zenikablogbackend.Representations.ArticlesRepresentation;
import com.zenika.zenikablogbackend.Repositories.Articles;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
public class RepresentationsConverter {

    private TagsRepository tagsRepository;

    public RepresentationsConverter(TagsRepository tagsRepository) {

        this.tagsRepository = tagsRepository;
    }

    public Iterable<ArticlesRepresentation> convertListOfArticlesToReprensentations(Iterable<Articles> articles) {
        List<ArticlesRepresentation> articlesRepresentations = new ArrayList<>();
        for (Articles a : articles) {
            articlesRepresentations.add(new ArticlesRepresentation(
                    a.getId(),
                    a.getUsers_id(),
                    a.getPublicCreator(),
                    a.getState(),
                    a.getTitle(),
                    a.getContent(),
                    a.getSummary(),
                    a.getImageUrl(),
                    a.getWordsInContent(),
                    convertTagsToListOfStrings(a.getTagsAssigned())
            ));
        }
        return articlesRepresentations;
    }

    public List<Tags> convertListOfStringToListOfTags(String stringList) {
        List<Tags> tagsList = new ArrayList<>();
        List<String> alreadyAddedStrings = new ArrayList<>();
        if (stringList != null) {
            String[] arr = stringList.split(" ");
            for (String str : arr) {
                if (!alreadyAddedStrings.contains(str)) {
                    if (tagsRepository.findByName(str) != null) {
                        tagsList.add(tagsRepository.findByName(str));
                    } else {
                        tagsRepository.save(new Tags(UUID.randomUUID().toString(), str));
                        tagsList.add(tagsRepository.findByName(str));
                    }
                    alreadyAddedStrings.add(str);
                }
            }
        }
        return tagsList;
    }

    public String convertTagsToListOfStrings(List<Tags> tagsList) {
        StringBuilder stringList = new StringBuilder();
        for (Tags tag : tagsList) {
            stringList.append(" ").append(tag.getName());
        }
        return stringList.toString();
    }

    public ArticlesRepresentation convertArticleToReprensentation(Articles articles) {
        return new ArticlesRepresentation(
                articles.getId(), articles.getUsers_id(), articles.getPublicCreator(), articles.getState(),
                articles.getTitle(), articles.getContent(), articles.getSummary(),
                articles.getImageUrl(), articles.getWordsInContent(), convertTagsToListOfStrings(articles.getTagsAssigned()));
    }

    public Iterable<Articles> convertListOfArticlesReprensentationstoArticles(Iterable<ArticlesRepresentation> articlesRepresentations) {
        List<Articles> articles = new ArrayList<>();
        for (ArticlesRepresentation a : articlesRepresentations) {
            articles.add(convertArticleRepresentationToArticle(a));
        }
        return articles;
    }

    public Articles convertArticleRepresentationToArticle(ArticlesRepresentation articlesRepresentation) {
        if (articlesRepresentation.getId() == null) {
            articlesRepresentation.setId(UUID.randomUUID().toString());
        }
        if (articlesRepresentation.getPublicCreator() == null
                || articlesRepresentation.getPublicCreator().equals("")
                || articlesRepresentation.getPublicCreator().equals(" ")
                || articlesRepresentation.getPublicCreator().equals("Auteur inconnu")) {
            articlesRepresentation.setUser_id("d90180fa-cce3-4e61-a880-ed4d0fd0ab53");
            articlesRepresentation.setPublicCreator("Auteur inconnu");
        }
        System.out.println(articlesRepresentation.getTagsAssigned());
        return new Articles(
                articlesRepresentation.getId(), articlesRepresentation.getUser_id(), articlesRepresentation.getPublicCreator(), articlesRepresentation.getState(),
                articlesRepresentation.getTitle(), articlesRepresentation.getContent(), articlesRepresentation.getSummary(),
                articlesRepresentation.getImageUrl(), getNumberOfWordsInArticleContent(articlesRepresentation.getContent()), convertListOfStringToListOfTags(articlesRepresentation.getTagsAssigned()));
    }

    public int getNumberOfWordsInArticleContent(String content) {
        int numberOfWords = 0;
        if (content != null) {
            String[] arr = content.split(" ");
            for (String str : arr) {
                numberOfWords++;
            }
        }
        return numberOfWords;
    }

}
